﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // get rigidbody2d
    private Rigidbody2D rgb;

    public GameObject flashlight;

    // get axis
    float moveHorizontal;
    float moveVertical;

    // move speed
    [SerializeField]
    private float speed = 20f;
    


    // Start is called before the first frame update
    void Start()
    {
        rgb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");
        rgb.velocity = new Vector2(speed * moveHorizontal, speed * moveVertical);

        if (moveHorizontal > 0)
        {
            flashlight.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90));
        }

        if (moveHorizontal < 0)
        {
            flashlight.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
        }

        if (moveVertical > 0)
        {
            flashlight.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }

        if (moveVertical < 0)
        {
            flashlight.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
        }

    }
}
