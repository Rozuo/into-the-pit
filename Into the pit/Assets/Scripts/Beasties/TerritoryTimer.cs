﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerritoryTimer : MonoBehaviour
{
    private Renderer render;
    float CurrentTime;
    float finalTime = 5f;

    float startTime;

    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        startTime = Time.time;
    }
    void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log(startTime);
        

        if(Time.time - startTime>= finalTime)
        {    
            render.material.color = Color.red;
        }
        
    }
}
