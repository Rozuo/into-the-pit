﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mutationManager : MonoBehaviour
{

    //starting color
    public Image meter;

    private Color32 mutationRate;
    private Color32 maxMutation = new Color32(52, 33, 69, 255);

    private List<Color32> stageOfMutation = new List<Color32>();
   

    private GameObject player;

    [Range(0f,100f)]
    public float mutation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        stageOfMutation.Add(new Color32(185, 191, 158, 255));
        stageOfMutation.Add(new Color32(118, 125, 89, 255));
        stageOfMutation.Add(new Color32(134, 110, 87, 255));
        player = GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if ( mutation >= 90f)
        {
            Debug.Log("I'm dead pepeHands");
            Destroy(gameObject);
        }
        else if( mutation >= 20f && mutation <= 40f)
        {
            meter.color = stageOfMutation[0];
        }
        else if (mutation > 40f && mutation <= 60f)
        {
            meter.color = stageOfMutation[1];
        }
        else if (mutation > 60f && mutation <= 80f)
        {
            meter.color = stageOfMutation[2];
        }

    }

    /*private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Infection Rad BB"))
        {
            Debug.Log("im dying halp");
            mutation += 1;
            meter.color = Color32.Lerp(meter.color, new Color32(52, 33, 69, 255), 0.000001f);
        }
    }*/

        //the big gey

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Basic Bitch"))
        {
            mutation += 20;
           // meter.color = Color32.Lerp(meter.color, maxMutation, Time.time);
        }
        else if (collision.gameObject.CompareTag("Hunter"))
        {
            mutation += 100;
        }
    }


}
