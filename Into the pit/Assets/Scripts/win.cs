﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class win : MonoBehaviour
{

    private Generator_functionality canWin;

    // Start is called before the first frame update
    void Start()
    {
        canWin = GetComponent<Generator_functionality>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("init");
        if (collision.gameObject.CompareTag("Win"))
        {
            Debug.Log("tag");
            if (canWin.winCondition)
            {
                Debug.Log("quit");

                this.enabled = false;
                Application.Quit();
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#endif
            }
        }

    }
}
