﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class HunterAI : MonoBehaviour
{

    public Transform target;
    public Transform gfx;

    public float speed = 20f;
    public float nextWaypointDist = 3f;
    public float radius = 10f;

    private float zRotation;

    Path path;
    int currentWaypoint = 0;
    bool reachedEnd = false;

    Seeker seeker;
    Rigidbody2D rgb;

    private bool seePlayer = false;

    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>();
        rgb = GetComponent<Rigidbody2D>();


        InvokeRepeating("updatePath", 0f, .5f);
    }

    private void Update()
    {

    }

    void updatePath()
    {
        if(seeker.IsDone())
            seeker.StartPath(rgb.position, target.position, onPathComplete);
    }

    void onPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (path == null)
            return;

        if(currentWaypoint >= path.vectorPath.Count){
            reachedEnd = true;
            return;
        }
        else
        {
            reachedEnd = false;
        }
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rgb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rgb.AddForce(force);

        float distance = Vector2.Distance(rgb.position, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDist)
        {
            currentWaypoint++;
        }

        zRotation = Mathf.Sqrt((Mathf.Pow(transform.position.x, 2) + Mathf.Pow(transform.position.y, 2)));

    }
}
