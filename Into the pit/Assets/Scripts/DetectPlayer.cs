﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private Color Old_Color = Color.yellow;
    void OnTriggerEnter2D(Collider2D other)
    {
        Renderer render = GetComponent<Renderer>();

        Old_Color = render.material.color;
        render.material.color = Color.black;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Renderer render = GetComponent<Renderer>();
        render.material.color = Old_Color;
    }
}